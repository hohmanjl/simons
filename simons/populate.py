#! /usr/bin/env python

from json import loads
import warnings
import logging
import sys
import pandas as pd

# Activate Django environment
import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')
import django
django.setup()
from simons.models import Gene

__author__ = 'jhohman'

# Configure logging
log = logging.getLogger()
log.setLevel(logging.DEBUG)
sh = logging.StreamHandler(sys.stdout)
sh.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)
sh.setFormatter(formatter)
log.addHandler(sh)


def main():
    filename = 'refGeneData.json'

    with open(filename, 'r') as f:
        data = loads(f.read())

    if not data:
        raise IOError('json data empty or unable to import json data.')

    count = data.get('count')
    results = data.get('results')

    imported_records = len(results)
    if count == imported_records:
        log.info('Import Success!')
    else:
        warnings.warn(
            'Number of imported records differs from internal count.',
            RuntimeWarning
        )

    log.info('Records imported/count: %d/%d.', imported_records, count)

    df = pd.DataFrame(results)

    log.info('Statistics (Max): \n%s', df.max())
    log.info('Longest String chrom: %d', df['chrom'].str.len().max())
    log.info('Longest String name: %d', df['name'].str.len().max())

    clean_gene = Gene.objects.all().delete()
    log.info('Cleaned existing Gene objects: %s.', clean_gene)

    gene_model = map(
        lambda x, g=Gene: g(
            tx_start=x['txStart'], tx_end=x['txEnd'],
            chrom=x['chrom'], name=x['name']
        ), results
    )

    gene_result = Gene.objects.bulk_create(gene_model)
    if count != len(gene_result):
        warnings.warn(
            'Number of created genes differs from imported count: %d/%d.'
            % (len(gene_result), count),
            RuntimeWarning
        )

    log.info(
        'Complete. %d Genes imported, %d Genes created.',
        count,
        len(gene_result),
    )


if __name__ == '__main__':
    main()
