"""
REST library for core application.

Reference: http://www.django-rest-framework.org/#installation
"""
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets
from django.conf import settings

__author__ = 'jhohman'

QUERY_LIMIT = getattr(settings, 'QUERY_LIMIT', 1000)


# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff')


# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()[:QUERY_LIMIT]
    serializer_class = UserSerializer


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
