"""core URL Configuration"""
from django.conf.urls import include, url
from django.contrib import admin
from core import rest_urls

urlpatterns = [
    url(r'', include('simons.urls', namespace='simons')),
    url(r'^rest/', include(rest_urls, namespace='rest_api')),
    url(r'^admin/', admin.site.urls),
]
