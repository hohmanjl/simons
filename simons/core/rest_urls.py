"""
core rest_urls Configuration

This way all the rest API is in one place.
"""
from django.conf.urls import include, url
from core.rest import router as rest_core_router
from simons.rest import GeneSearchList

urlpatterns = [
    url(r'', include(rest_core_router.urls)),
    url(
        r'^genes/by-name/(?P<term>[\S]+)/$',
        GeneSearchList.as_view(),
        kwargs=dict(field='name')
    ),
    url(
        r'^genes/by-chrom/(?P<term>[\S]+)/$',
        GeneSearchList.as_view(),
        kwargs=dict(field='chrom')
    ),
    url(
        r'^genes/by-both/(?P<term>[\S]+)/$',
        GeneSearchList.as_view(),
        kwargs=dict(field='both')
    ),
    # No term in url, just fallback to search all.
    url(
        r'^genes/(by-name|by-chrom|by-both)/',
        GeneSearchList.as_view(),
        kwargs=dict(field='both')
    ),
    url(
        r'^api-auth/',
        include('rest_framework.urls', namespace='rest_framework')
    )
]
