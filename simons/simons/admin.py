from django.contrib import admin
from simons.models import Gene

__author__ = 'jhohman'


class GeneAdmin(admin.ModelAdmin):
    list_display = ('id', 'chrom', 'name', 'tx_start', 'tx_end')
    list_filter = ('chrom',)


admin.site.register(Gene, GeneAdmin)
