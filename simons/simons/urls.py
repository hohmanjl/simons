from django.conf.urls import url
from simons import views

__author__ = 'jhohman'


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(
        r'^gene_search/$',
        views.search, kwargs={'field': 'both'}, name='search'
    ),
    url(
        r'^gene_search/by-name/(?P<term>.*)/$',
        views.search, kwargs={'field': 'name'}, name='search_name'
    ),
    url(
        r'^gene_search/by-chrom/(?P<term>.*)/$',
        views.search, kwargs={'field': 'chrom'}, name='search_chrom'
    ),
    url(
        r'^gene_search/by-both/(?P<term>.*)/$',
        views.search, kwargs={'field': 'both'}, name='search_both'),
]
