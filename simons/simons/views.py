import logging
from django.shortcuts import render
from django.http import Http404

__author__ = 'jhohman'

log = logging.getLogger(__name__)


def index(request):
    log.info('index view')
    template = 'simons/index.html'
    extra_context = dict(angular=False, debug=False)
    return render(request, template, extra_context)


def search(request, field=None, term=''):
    log.info('search view, field: "%s", term: "%s"' % (str(field), term))
    print ('search view, field: "%s", term: "%s"' % (str(field), term))

    if field is None:
        """
        We consider the field a requirement.
        This is not insecure. Although we use field to generate a
        dynamic query, field values are given from our url mappings.
        That is, we are not taking untrusted input. We could check
        membership for white-list values, but I think that
        is redundant here.
        """
        raise Http404

    template = 'simons/gene_search.html'
    extra_context = dict(
        term=term,
        field=field,
        angular=True,
        debug=False
    )

    return render(request, template, extra_context)
