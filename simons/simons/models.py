"""
simons app models.py
Author: James Hohman
Date: 2016-01-26
"""
from django.db import models

__author__ = 'jhohman'


class Gene(models.Model):
    chrom = models.CharField(max_length=32)
    name = models.CharField(max_length=32)
    tx_start = models.PositiveIntegerField()
    tx_end = models.PositiveIntegerField()
