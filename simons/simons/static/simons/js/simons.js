/**
 * simons.js
 *
 * jQuery
 * @author James Hohman
 * @requires jQuery 1.11.3 or later
 *
 * Copyright (c) 2016, James Hohman
 *
 */

$(document).ready(function() {

});


(function (simons, $, undefined) {
  simons.search_input_id = '#gs-term';

  simons.search = function(field) {
    field = field === undefined ? "both" : field;
    var term = encodeURIComponent($(simons.search_input_id).val());

    if (term) {
      switch(field) {
        case('name'):
          window.location.href = "/gene_search/by-name/" + term + "/";
          break;
        case('chrom'):
          window.location.href = "/gene_search/by-chrom/" + term + "/";
          break;
        default:
          window.location.href = "/gene_search/by-both/" + term + "/";
      }
    } else {
      // If the user didn't specify a term, all we can do is return
      // the top n results.
      window.location.href = "/gene_search/";
    }
  };
}(window.simons = window.simons || {}, jQuery));

// Initialize module with custom pagination scheme
// http://jakelitwicki.com/2015/07/30/custom-angular-smart-table-pagination-template/
var sApp = angular.module('sApp', ['ui.bootstrap', 'smart-table']).run(['$templateCache', function ($templateCache) {
 $templateCache.put('template/smart-table/pagination.html',
 '<p class="pagination-summary">' +
 'Loaded {{ totalItemCount }} out of {{ $parent.response.data.count }} matching records.' +
 '<span ng-if="$parent.response.data.count > 0" ng-scope><br>Displaying records {{ stItemsByPage * (currentPage - 1) + 1 + $parent.$parent.offset }} - {{ stItemsByPage * currentPage + $parent.$parent.offset  }}</spanng-if>' +
 '</p>' +
 '<nav ng-if="numPages && pages.length >= 2"><ul class="pagination" ng-init="parent = $parent.$parent">' +
 '<li ng-class="" ng-if="parent.response.data.previous !== null" class="pagination-btn-end" title="Fetch previous set from server"><a ng-click="parent.fetch(parent.response.data.previous)"><i class="fa fa-fw fa-angle-double-left"></i><i class="fa fa-fw fa-cloud-download"></i></a></li>' +
 '<li ng-class="" ng-if="parent.response.data.previous === null" class="pagination-btn-end disabled" title="Fetch previous set from server"><a disabled><i class="fa fa-fw fa-angle-double-left" disabled></i><i class="fa fa-fw fa-cloud-download" disabled></i></a></li>' +
 '<li ng-class="" class="pagination-btn-end" title="Go to first page"><a ng-click="selectPage(1)"><i class="fa fa-fw fa-angle-double-left"></i></a></li>' +
 '<li ng-class="" title="Go to previous page"><a ng-click="selectPage(currentPage-1)"><i class="fa fa-fw fa-angle-left"></i></a></li>' +
 '<li ng-repeat="page in pages" ng-class="{active: page==currentPage}"><a ng-click="selectPage(page)">{{ page }}</a></li>' +
 '<li ng-class="" title="Go to next page"><a ng-click="selectPage(currentPage+1)"><i class="fa fa-fw fa-angle-right"></i></a></li>' +
 '<li ng-class="" class="pagination-btn-end" title="Go to last page"><a ng-click="selectPage(numPages)"><i class="fa fa-fw fa-angle-double-right"></i></a></li></a></li>' +
 '<li ng-class="" ng-if="parent.response.data.next !== null" class="pagination-btn-end" title="Fetch next set from server"><a ng-click="parent.fetch(parent.response.data.next)"><i class="fa fa-fw fa-cloud-download"></i><i class="fa fa-fw fa-angle-double-right"></i></a></li></a></li>' +
 '<li ng-class="" ng-if="parent.response.data.next === null" class="pagination-btn-end disabled" title="Fetch next set from server"><a disabled><i class="fa fa-fw fa-cloud-download" disabled></i><i class="fa fa-fw fa-angle-double-right" disabled></i></a></li></a></li>' +
 '</ul></nav>');
}]);

sApp.filter('title', function() {
  return function(input, scope) {
    if (input) {
      return input.substring(0, 1).toUpperCase() + input.substring(1);
    }
  }
});

sApp.controller('SearchController', [
  '$scope', '$http', '$location',
  function($scope, $http, $location) {
  // Angular controller that handles the search functionality.
  console.log('Initialize SearchController');

  $scope.term = null;
  $scope.response = null;
  $scope.results_clone = null;
  $scope.params = {};
  $scope.debug = false;
  $scope.in_transit = true; // prevent spurious error messaging while ajax in progress.
  $scope.initializing = true; // Use this to show the large "fetching records"

  var request_json_format = '/?format=json';
  var search_all_url = '/rest/genes/by-both' + request_json_format;

  $scope.$watch('$viewContentLoaded', function () {
    // load the initial configuration.
    $scope.params.term = $('#gs-term').val();
    $scope.params.field = $('#gs-field').val();

    // console.log('Page initialized.');
    // console.log('params:');
    // console.log($scope.params);

    // Fire off the ajax request for data.
    $scope.search($scope.params.field);
  });

  var rest_wrapper = function(url) {
    console.log(url);

    // Sanity check, ensure url is not null or undefined.
    if (!url) {
      throw new Error('url must not be null or undefined!');
    }
    $scope.in_transit = true;

    query_parse(url);

    var request = $http({
      method: 'GET',
      url: url
    }).then(response_handler, response_handler);

    request.then(function () {
      $scope.in_transit = false;
      $scope.initializing = false;
    });
  };

  var response_handler = function(response) {
    if (response.status == 200) {
      console.log('rest response: ');
      console.log(response);
      // stSafeSrc attribute required on smart-table for ajax loaded data.
      // It requires a "separate" collection. Thus, to prevent
      // pass by reference, we do a clone operation on the array.
      $scope.response = response;
      $scope.results_clone = response.data.results.slice(0);
    } else {
      console.log('rest error: ');
      console.log(response);
      $scope.response = response;
      $scope.results_clone = [];
    }
  };

  var term_handler = function(search_term) {
    if (search_term) {
      return true;
    } else {
      // If search term is blank, then just return top n results.
      rest_wrapper(search_all_url);
      return false;
    }
  };

  var query_parse = function (url) {
    // Parse url for query parameters
    var query = $location.search(url);
    // console.log('query: ');
    // console.log(query);

    var q_search = false;
    var limit = "";
    var offset = "";

    if (query.hasOwnProperty('$$search')) {
      q_search = query['$$search'];
    }

    if (q_search.hasOwnProperty('limit')) {
      limit = q_search['limit'];
    } else {
      limit = "0";
    }
    if (q_search.hasOwnProperty('offset')) {
      offset = q_search['offset'];
    } else {
      offset = "0";
    }

    $scope.limit = parseInt(limit);
    $scope.offset = parseInt(offset);
  };

  $scope.search = function(field) {
    var term = $scope.params.term;
    var field_api_url = "";  // Initialize so we don't have multiple declarations.

    switch(field) {
      case('name'):
        $scope.params.field = 'name';
        field_api_url = "/rest/genes/by-name/";
        break;
      case('chrom'):
        $scope.params.field = 'chrom';
        field_api_url = "/rest/genes/by-chrom/";
        break;
      default:
        $scope.params.field = 'both';
        field_api_url = "/rest/genes/by-both/";
    }

    if (term_handler(term)) {
      var url = field_api_url + term + request_json_format;
      rest_wrapper(url);
    }
  };

  $scope.fetch = function(url) {
    // console.log('fetch: ' + url);
    // Publically exposed wrapper function to make server calls.
    rest_wrapper(url);
  };
}]);
