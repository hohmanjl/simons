"""
REST library for simons application.
"""
from simons.models import Gene
from rest_framework import serializers, viewsets
from django.http import Http404
from rest_framework.generics import ListAPIView
from django.db.models import Q

__author__ = 'jhohman'


class GeneSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Gene
        fields = ('id', 'chrom', 'name', 'tx_start', 'tx_end')


class GeneViewSet(viewsets.ModelViewSet):
    queryset = Gene.objects.all()
    serializer_class = GeneSerializer


class GeneSearchList(ListAPIView):
    """
    Retrieve a set of genes.
    """
    serializer_class = GeneSerializer

    def get_queryset(self):
        field = self.kwargs.get('field', None)
        term = self.kwargs.get('term', None)

        if term is None:
            # If term is not provided all we can do is just return the
            # top n results.
            return Gene.objects.all()

        if field is None:
            """
            We consider the field a requirement.
            This is not insecure. Although we use field to generate a
            dynamic query, field values are given from our url mappings.
            That is, we are not taking untrusted input. We could check
            membership for white-list values, but I think that
            is redundant here.
            """
            raise Http404

        if "'" in term or '"' in term:
            match = 'iexact'
            # ToDo: Replace with a more robust "unquoting" scheme.
            # This is cheap and will work since we don't expect users to search
            # any text that contains quotes.
            term = term.strip('"').strip("'").strip('"')
        else:
            match = 'icontains'

        if field is 'both':
            q_name = {'name__%s' % match: term}
            q_chrom = {'chrom__%s' % match: term}
            queryset = Gene.objects.filter(
                Q(**q_name) | Q(**q_chrom)
            )
        else:
            query_parameter = '%s__%s' % (field, match)
            query = {query_parameter: term}
            queryset = Gene.objects.filter(**query)

        return queryset
