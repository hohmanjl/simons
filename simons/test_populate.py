#! /usr/bin/env python

from django.test import TestCase
from populate import main
from simons.models import Gene


class TestPopulate(TestCase):
    def test_json_object_creation(self):
        """Test db population script."""
        import_count = 53293

        # Assert precondition
        self.assertEqual(0, Gene.objects.count())
        main()

        # Assert postcondition
        self.assertEqual(import_count, Gene.objects.count())
