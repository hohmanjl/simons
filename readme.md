# Simons Project
### A Django Site Example

## Purpose
This is a demo of full-stack web development, that integrates with an 
 existing site design.

## Version Info
Version: 0.9

Date: 1/28/2016

## Author
 * James Hohman [Bitbucket](https://bitbucket.org/TechMedicNYC/)

## Citations
 * Original Materials: [Simons Foundation](https://www.simonsfoundation.org/)

## Requirements
 * Python 2.7.10
 * Django 1.9.1
 * Angular.js 1.4.5
 * Bootstrap 3.3.6

## License, Copyright
This software is intended for demonstration only. Portions of 
the Simons Foundation site was used with permission.

## Main packages
| path | description |
-------|--------------
| . | Repo/Project files. |
| simons/ | Simons Django project |
| simons/core/ | Core configuration |
| simons/simons/ | The simons application |


## Quick Start Guide

To fire up this project:

1. Pull the simons repo [https://TechMedicNYC@bitbucket.org/TechMedicNYC/simons.git](https://TechMedicNYC@bitbucket.org/TechMedicNYC/simons.git)
2. Preferably in a `virtualenv`, install dependencies:
  
        pip install -r requirements.txt
  
3. Apply migrations.

        $ python manage.py migrate
        
4. (Optional) If you would like to use the administration system, create a 
project superuser.

        $ python manage.py createsuperuser
        
5. Populate the database.
  
        $ cd simons/
        $ python populate.py
  
6. Start the web server:
  
        $ python manage.py runserver
        
---

To test:

    $ python manage.py test 


## Business Requirements
### Part I
- [x] Create repo
- [x] Integrate with existing UI/Design
- [x] Create/populate database with json data set
- [ ] Use MySQL/PostgreSQL Database
- [x] Use Django Framework
- [x] Search functionality by `chrom` or `name`
- [x] Incorporate search into existing site
- [x] Create RESTful API
- [x] Call REST API via AJAX Loads, without page reload
- [x] Make page/UI aesthetically pleasing
- [x] Incorporate pagination to search results
- [x] Make search results sortable
- [x] Pages built from template
- [x] Include CSS styling
### Part II
- [x] Include `readme.txt` (`readme.md`, this document)
- [x] "How to run application" documentation.
- [x] Include `requirements.txt`.


## Commentary

### [Commit d105eb2](https://bitbucket.org/TechMedicNYC/simons/commits/d105eb2974cd795e36e51e1b4403ac8d785591da)
> **Build prototype** django project that incorporates existing site UI.
Collect and acquire all templates and static content. Transform rendered
templates to be compatible with Django's templating framework. Inheritance and 
inclusion is a powerful feature of the Django framework that
facilitates DRY and logical organization of application templates.

> Rather than build a one-off style system I attempt to integrate bootstrap
into the overall design.

> **Prototype a UI/UX scheme.** Overall idea is to create a "Gene Search" page,
that is identical to the [current site search design]
(https://www.simonsfoundation.org/?s=cats#). Furthermore, 
we want to use the same search widget (DRY) from the main page and the 
gene search page. To accomplish this we will rely on Django to signal to 
the page that it is angular enabled, changing the way the template interacts
with the controller (Angular or jQuery).

> To meet dynamic data requirements, I could build a UI from scratch using 
jQuery/javascript; however, no need to reinvent the wheel. Let's use
[Angular.js](https://angularjs.org/) as this makes it trivial to meet 
most of the business requirements, such as: ajax loads, model synchronization 
and representation (template), and workflow.

> **Created `populate.py` to initialize the database** with the json data.
In older versions of Django, we could import json fixtures--not sure about 
v1.9.x--but even then fixture loading was frowned upon. Thus, it is not 
difficult to write a simple parsing script.

> Next, I used [Pandas](http://pandas.pydata.org/) to quickly acquire some 
statistics about the data set. I used these statistics to determine my models. 
Thus, Pandas (being a large library) is not really necessary for this 
project and those parts can be commented out to make the demo more lightweight.

### [Commit d7a596d](https://bitbucket.org/TechMedicNYC/simons/commits/d7a596d6b19f03b1250f09c16d53bfed68610916)
> **Added prototype RESTful API** using [Django REST framework]
(http://www.django-rest-framework.org/). Have to read the 
docs and apply to meet the objective.

> **URL Scheme is a little funny looking**, but the overall concept is that all
REST API calls will happen under `/`*`domain`*`/rest/`. There might be a better way 
to set up the urls, but this suffices for now:

    urlpatterns = [
        ...
        url(r'^rest/', include(rest_core_router.urls)),
        url(r'^rest/', include(rest_simons_router.urls)),
        
### [Commit e429b82](https://bitbucket.org/TechMedicNYC/simons/commits/e429b82ebee7f962327e636fcf669038400c4ba4)

> Added commentary, business requirements to `readme.md`. 

> **Segmented Django search view and Angular search controller.** Technically, 
all searches are performed via Ajax calls to the REST API by the Angular 
controller. The Django view simply prepopulates the template with search 
parameters to which the Angular controller initializes and fetches the 
initial search result.

> This isolates the search logic to the Angular controller--single use 
principle--and makes the search page more robust as it can be seeded by 
parameters server-side via the Django view.

### [Commit 21f7691](https://bitbucket.org/TechMedicNYC/simons/commits/21f76914b053dc738a8ca3467683a9106c608a63)

> **Refactored search functions** into one function. Centralized results 
initialization and REST API handling.

> **Added front end error messaging.** Handles non-200 status messages dynamically.
Messaging is therefore a little ambiguous because of the all-in-one error 
messaging. This could be expanded upon to provide discrete error messages for
various error codes. Future.

> **Added Q objects for the 'both' search filter**, 'name' and 'chrom'.

> **Added pagination to the REST API.** This makes the QUERY_LIMIT parameter 
somewhat obsolete. However, I will try to improve the functionality of the 
pagination in the next commit.

> **Utilize ajax promise,** to mark begin/end of transmission to eliminate 
spurious error messaging to the user.

> **Add debug parameter to templates** for verbose template output. Did not 
tie this to settings.DEBUG because I still need to be in a Django debug 
environment, but want to be able to see the template as intended.

### [Commit 8c8c01e](https://bitbucket.org/TechMedicNYC/simons/commits/8c8c01edcdfe76349bd1b01d1c483669a0f99d75)

> **REST API Fixup.** Cleaned up REST API calls. Now there is only one gene
api. `settings.QUERY_LIMIT` has been supplanted by the api pagination scheme. 
REST urls have been migrated to their own module for better maintenance and
organization. The entire rest library now rests under `/domain/rest/`.

> **Improved pagination template.** Has relative navigation and first/last 
page navigation. Displays detailed statistics of the record you are viewing.

> **Improved tabular view.** Displays absolute gene id (db primary key) for 
unambiguous reference.

> **Added response_handler.** Centralized logic to handle responses,
 eliminates need for discrete variables. Just hand the response to the template.
 
> **Improved Search Syntax.** Quoting the search term results in an exact 
lookup. Unquoted search terms perform a membership lookup.

### [Commit da4b379](https://bitbucket.org/TechMedicNYC/simons/commits/da4b379cf72a59d6fc532d176389c288b52e9496)

> **Added next/prev pagination server fetch.** Full search set navigation.

> **Converted site search form** to bootstrap. Issues with responsive 
layout. Future.

> **Improved `smart-table` template.** Ideally, I'd like to show disabled
UI elements to preserve consistent UI appearance, and UI clues for the 
user. However, `ng-disabled` was not eliminating the on-click events for the
UI element. Furthermore, the disabled property did not propagate to child 
elements. Thus, a bit of a hack, but we use ng-if to display enabled 
control, and another ng-if to display a disabled control. Short-term 
solution.

> **Improved "Fetching Records UX."**

### [Commit d6631a3](https://bitbucket.org/TechMedicNYC/simons/commits/d6631a3a2a17d6b6e81c14c5c6a0450e7e97ed75)

> **Fixed critical bug in `populate.py`.**

> **Added `Quick Start Guide`.**

> **Added table hover effect.**

## Summary
The search widget meets all business requirements, but likely some 
functionality could be improved. Need to add unit tests and selenium / front
end / integration tests. It is very necessary to build out some integration 
tests that mock different REST responses, such as 400, 403, 404, 500. To see
text examples look at my other project [testParseExample](https://bitbucket.org/TechMedicNYC/textparseexample).

There exist some UI issues, such as the mis-sized site search bar and the
responsive layout has been compromised. These did not go unnoticed. 
However, I spent my time building out a comprehensive search application and 
ensuring that it was relatively bug free (manual testing).

One of the difficulties with this type of development is integrating all of 
the css rules and js applications with the existing page design. Since this 
page contains many legacy components, more time would be needed to 
properly integrate the new functionality into the existing design. This 
can be accomplished, I simply prioritized comprehensive functionality over 
aesthetics for this project and deadline.

Regarding the overall design, I chose firebrick/red for a header color for 
search results as the overall theme is navy and yellow. A third color 
which complements the other two is okay in my book. Furthermore, since 
red is not used on the site the users' focus is drawn to the search results.

The UI pagination controls are a slight modification from stock. Namely, 
I wanted the first/last controls, as well as prev/next pagination. However,
we also needed a way to fetch more records from the server for large 
result sets. Therefore, I decided to use a "cloud" icon, indicating this 
fetch will pull from the internet. Finally, I added increasing space 
between the main pagination controls and the first/last and server fetch 
controls. This is a subtle visual indication that these actions are farther 
and farther removed from the rather narrow navigation of the main 
pagination.

Next, I wanted to include item numbers in the result set, but without modifying 
the result array in place, this is not possible. We *can* add a pseudo field,
`result.indexOf(row)` however, there are several drawbacks: 1) The pseudo field 
cannot be searched or sorted. 2) The pseudo field re-arranges index on table 
sort. 3) The table becomes more crowded, and the extra numbers don't really 
add any clarity as to what the user is looking at.

Therefore, I compromised and made a non-invasive list of the result statistics
beneath the table. I also used `x-small` fonts to minimize their visual impact.
Helpful, without being distracting or obtrusive.

Initially, I intended to show the "Fetching Records" element every time we 
submitted an ajax request. However, showing and hiding the table data 
was visually jarring and I did not like it. Thus, the large "Fetching 
Records" only appears on initialization. For all requests after that, 
we only display small, unobtrusive text near the "Gene Search" header.
